package com.example.ekahauexam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ekahau_solution.EkahauSolution
import com.example.grouping_exam.DataSource

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DataSource.emit().subscribe(EkahauSolution()::solve)
    }
}
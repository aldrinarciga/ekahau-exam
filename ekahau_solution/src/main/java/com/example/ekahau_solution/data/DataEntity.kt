package com.example.ekahau_solution.data

internal sealed class DataEntity(
    var data: String,
    var created: Int = 0,
    var updated: Int = 0,
) {

    object Repository: DataEntity(data = "R")

    class Box(
        data: String,
        step: Int
    ): DataEntity(data, step, step)

    class Folder(
        data: String,
        step: Int
    ): DataEntity(data, step, step)

    class Paper(
        data: String,
        step: Int
    ): DataEntity(data, step, step)

    override fun equals(other: Any?): Boolean {
        return (other as? DataEntity)?.data?.equals(data) == true
    }
}
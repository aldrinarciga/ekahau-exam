package com.example.ekahau_solution.data

internal class TreeNode<T>(
    val value: T,
    var parent: TreeNode<T>? = null,
    val children: ArrayList<TreeNode<T>> = arrayListOf()
) {

    fun addChild(node: TreeNode<T>) {
        node.parent = this
        if (findChild(node.value) == null) {
            children.add(node)
        }
    }

    fun setNewParent(parent: TreeNode<T>) {
        this.parent?.removeChild(value)
        parent.addChild(this)
    }

    fun removeChild(value: T) = findChild(value)?.also {
        children.remove(it)
    }

    private fun findChild(value: T): TreeNode<T>? = children.find { it.value == value }

    fun hasChildren() = children.isNotEmpty()
}
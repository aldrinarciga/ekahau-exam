package com.example.ekahau_solution.util

import com.example.ekahau_solution.data.DataEntity
import com.example.ekahau_solution.data.TreeNode

@Suppress("UNCHECKED_CAST")
internal fun List<String>.inputToBoxRepo(step: Int): TreeNode<DataEntity> {
    val boxRepo = TreeNode<DataEntity>(value = DataEntity.Repository)
    var currentBox: TreeNode<DataEntity>? = null
    var currentFolder: TreeNode<DataEntity>? = null
    reversed().forEach {
        when {
            it.startsWith("B") -> {
                currentBox = TreeNode<DataEntity>(
                    value = DataEntity.Box(data = it, step = step)
                ).also { box ->
                    boxRepo.addChild(box)
                }
            }
            it.startsWith("F") -> {
                currentFolder = TreeNode<DataEntity>(
                    value = DataEntity.Folder(data = it, step = step)
                ).also { folder ->
                    currentBox?.addChild(folder)
                }
            }
            it.startsWith("P") -> {
                TreeNode<DataEntity>(
                    value = DataEntity.Paper(data = it, step = step)
                ).also { paper ->
                    currentFolder?.addChild(paper)
                }
            }
        }
    }

    return boxRepo
}
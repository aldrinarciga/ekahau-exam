package com.example.ekahau_solution

import android.util.Log
import com.example.ekahau_solution.data.DataEntity
import com.example.ekahau_solution.data.TreeNode
import com.example.ekahau_solution.util.inputToBoxRepo

class EkahauSolution {

    private val warehouse: TreeNode<DataEntity> = TreeNode(
        value = DataEntity.Repository
    )

    private var step = 0

    fun solve(input: List<String>) {
        input.inputToBoxRepo(step)
            .buildTreeToUse()
            .cleanTree()
            .addOrUpdate(warehouse)

        warehouse.cleanTree()
            .treeToList()
            .filter { it !is DataEntity.Repository }
            .joinToString(prefix = "[", postfix = "]") {
                "${it.data}:${step - it.updated}"
            }.run {
                Log.d("Output", this)
            }

        step++
    }

    /**
     * Builds an "addable" tree based on the existing [warehouse]
     */
    private fun TreeNode<DataEntity>.buildTreeToUse(): TreeNode<DataEntity> {
        children.forEach { box ->
            box.children.forEach { folder ->
                folder.children.mapNotNull {
                    warehouse.findDataByValue(it.value.data)?.parent
                }.maxByOrNull { it.value.created }?.also { newerFolder ->
                    folder.value.data = newerFolder.value.data
                    folder.parent?.value?.data = newerFolder.parent?.value?.data!!
                }
            }
        }
        return this
    }

    /**
     * Updates the class member [warehouse] with the given tree
     */
    private fun TreeNode<DataEntity>.addOrUpdate(parent: TreeNode<DataEntity>) {
        val existingNode = warehouse.findDataByValue(value.data)
        if (existingNode != null) {
            existingNode.value.updated = value.updated
            if (
                existingNode.parent?.value != parent.value &&
                existingNode.value !is DataEntity.Repository
            ) {
                existingNode.setNewParent(parent)
            }
            children.forEach {
                it.addOrUpdate(existingNode)
            }
        } else {
            parent.addChild(this)
        }
    }

    /**
     * Converts the given tree into a list
     */
    private fun TreeNode<DataEntity>.treeToList(): ArrayList<DataEntity> {
        val list = arrayListOf<DataEntity>()
        children.map {
            it.treeToList()
        }.forEach {
            list.addAll(it)
        }

        list.add(this.value)

        return list
    }

    /**
     * Traverse the tree to find a specific node
     * with the given queryValue
     *
     * @param queryValue the value to find
     */
    private fun TreeNode<DataEntity>.findDataByValue(
        queryValue: String
    ): TreeNode<DataEntity>? {
        return when {
            value.data == queryValue -> this
            hasChildren() -> {
                var ctr = 0
                do {
                    val result = children[ctr++].findDataByValue(queryValue)
                    if (result != null) {
                        return result
                    }
                } while (ctr < children.size)

                null
            }
            else -> null
        }
    }

    private fun TreeNode<DataEntity>.cleanTree(): TreeNode<DataEntity> {
        children.forEach { box ->
            box.children.filter { folder ->
                !folder.hasChildren()
            }.forEach { box.removeChild(it.value) }
        }
        children.filter { box ->
            !box.hasChildren()
        }.forEach { removeChild(it.value) }

        return this
    }

}